#ifndef MOBILEESCAPEROOM_HPP
#define MOBILEESCAPEROOM_HPP

#include "pigpiopp.h"
#include <vector>
#include <stdexcept>
#include <mutex>
#include <condition_variable>
#include <string>
#include <SFML/Audio.hpp>
#include <cstdint>
#include <iostream>

class MobileEscapeRoom
{
    std::mutex mutex;
    bool is_ready;
    std::condition_variable cv_ready;
    std::vector<unsigned> input_labyrinth;
    std::vector<bool> input_labyrinth_values;
    unsigned input_labyrinth_cheat; // Hidden button to solve labyrinth
    unsigned input_buzzer;
    unsigned input_reset;
    unsigned input_exit;
    unsigned output_labyrinth;
    unsigned output_buzzer;
    sf::SoundBuffer sound_buffer_buzzer;
    sf::Sound sound_buzzer;
    int riddles_solved;
    pigpiopp::client pigpio;

public:
    MobileEscapeRoom(const std::vector<unsigned> &input_labyrinth,
                     unsigned input_labyrinth_cheat,
                     unsigned input_buzzer,
                     unsigned input_reset,
                     unsigned input_exit,
                     unsigned output_labyrinth,
                     unsigned output_buzzer,
                     const std::string &sound_file_buzzer);

    void run();

private:
    void check_labyrinth();

    void reset();
};

#endif // MOBILEESCAPEROOM_HPP
