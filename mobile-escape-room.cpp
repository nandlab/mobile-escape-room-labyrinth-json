#include "mobile-escape-room.hpp"
#include <cstdint>
#include <iostream>

template<typename CharT, typename Traits, typename ElemT>
static std::basic_ostream<CharT, Traits>& operator <<(std::basic_ostream<CharT, Traits> &ostream, const std::vector<ElemT> &vector) {
    ostream << '[';
    bool first = true;
    for (const ElemT &e : vector) {
        if (!first) {
            ostream << ", ";
        }
        ostream << e;
        first = false;
    }
    return ostream << ']';
}

#define STEADY 25000

MobileEscapeRoom::MobileEscapeRoom(
        const std::vector<unsigned> &input_labyrinth,
        unsigned input_labyrinth_cheat,
        unsigned input_buzzer,
        unsigned input_reset,
        unsigned input_exit,
        unsigned output_labyrinth,
        unsigned output_buzzer,
        const std::string &sound_file_buzzer)
    : mutex()
    , is_ready(false)
    , cv_ready()
    , input_labyrinth(input_labyrinth)
    , input_labyrinth_values(input_labyrinth.size(), true)
    , input_labyrinth_cheat(input_labyrinth_cheat)
    , input_buzzer(input_buzzer)
    , input_reset(input_reset)
    , input_exit(input_exit)
    , output_labyrinth(output_labyrinth)
    , output_buzzer(output_buzzer)
    , sound_buffer_buzzer()
    , sound_buzzer()
    , riddles_solved(0)
    , pigpio()
{
    bool success = sound_buffer_buzzer.loadFromFile(sound_file_buzzer);
    if (!success) {
        throw std::runtime_error("Cannot load sound " + sound_file_buzzer);
    }
    sound_buzzer.setBuffer(sound_buffer_buzzer);
}

void MobileEscapeRoom::reset() {
    pigpio.write(output_labyrinth, 0);
    pigpio.write(output_buzzer, 0);
    sound_buzzer.stop();
    riddles_solved = 0;
}

void MobileEscapeRoom::check_labyrinth() {
    using std::cout;
    using std::endl;
    if (riddles_solved == 0) {
        // Check if all labyrinth pins are high
        bool all_low = true;
        for (bool val : input_labyrinth_values) {
            if (val) {
                all_low = false;
                break;
            }
        }
        if (all_low) {
            cout << "Labyrinth solved" << endl;
            pigpio.write(output_labyrinth, 1);
            riddles_solved++;
        }
    }
}


void MobileEscapeRoom::run() {
    using std::cout;
    using std::endl;
    using std::uint32_t;
    using std::size_t;
    std::unique_lock<std::mutex> lock(mutex);
    cout << "Starting mobile escape room program..." << endl;
    cout << "Input labyrinth: " << input_labyrinth << endl;
    cout << "Input labyrinth cheat: " << input_labyrinth_cheat << endl;
    cout << "Input buzzer: " << input_buzzer << endl;
    cout << "Reset pin: " << input_reset << endl;
    cout << "Exit pin: " << input_exit << endl;
    cout << "Output labyrinth: " << output_labyrinth  << endl;
    cout << "Output buzzer: " << output_buzzer  << endl;
    pigpio.start(nullptr, nullptr);

    pigpio.set_mode(output_labyrinth, pigpiopp::OUTPUT);
    pigpio.write(output_labyrinth, 0);

    pigpio.set_mode(output_buzzer, pigpiopp::OUTPUT);
    pigpio.write(output_buzzer, 0);

    for (size_t i = 0; i < input_labyrinth.size(); i++) {
        unsigned input_pin = input_labyrinth[i];
        pigpio.set_mode(input_pin, pigpiopp::INPUT);
        pigpio.set_glitch_filter(input_pin, STEADY);
        pigpio.register_callback(input_pin, pigpiopp::EITHER_EDGE, [this, i](int gpio, int level, uint32_t tick) {
            (void) gpio;
            (void) tick;
            std::lock_guard<std::mutex> lock(mutex);
            cout << "Labyrinth input " << i << " is " << (level ? "HIGH" : "LOW") << endl;
            input_labyrinth_values[i] = level;
            check_labyrinth();
        });
        // Get initial input pin value
        input_labyrinth_values[i] = pigpio.read(input_pin);
    }
    check_labyrinth();

    pigpio.set_mode(input_labyrinth_cheat, pigpiopp::INPUT);
    pigpio.set_glitch_filter(input_labyrinth_cheat, STEADY);
    pigpio.register_callback(input_labyrinth_cheat, pigpiopp::FALLING_EDGE, [this](int gpio, int level, uint32_t tick) {
        (void) gpio;
        (void) level;
        (void) tick;
        std::lock_guard<std::mutex> lock(mutex);
        cout << "Labyrinth cheat pressed" << endl;
        if (riddles_solved == 0) {
            cout << "Labyrinth solved by cheating" << endl;
            pigpio.write(output_labyrinth, 1);
            riddles_solved++;
        }
    });

    pigpio.set_mode(input_buzzer, pigpiopp::INPUT);
    pigpio.set_glitch_filter(input_buzzer, STEADY);
    pigpio.register_callback(input_buzzer, pigpiopp::FALLING_EDGE, [this](int gpio, int level, uint32_t tick) {
        (void) gpio;
        (void) level;
        (void) tick;
        std::lock_guard<std::mutex> lock(mutex);
        cout << "Buzzer pressed" << endl;
        if (riddles_solved == 1) {
            cout << "Buzzer solved" << endl;
            pigpio.write(output_buzzer, 1);
            sound_buzzer.play();
            riddles_solved++;
            cout << "All riddles solved, waiting for reset or exit..." << endl;
        }
    });

    pigpio.set_mode(input_reset, pigpiopp::INPUT);
    pigpio.set_glitch_filter(input_reset, STEADY);
    pigpio.register_callback(input_reset, pigpiopp::FALLING_EDGE, [this](int gpio, int level, uint32_t tick) {
        (void) gpio;
        (void) level;
        (void) tick;
        std::lock_guard<std::mutex> lock(mutex);
        cout << "Reset" << endl;
        reset();
        check_labyrinth();
    });

    pigpio.set_mode(input_exit, pigpiopp::INPUT);
    pigpio.set_glitch_filter(input_exit, STEADY);
    pigpio.register_callback(input_exit, pigpiopp::FALLING_EDGE, [this](int gpio, int level, uint32_t tick) {
        (void) gpio;
        (void) level;
        (void) tick;
        std::lock_guard<std::mutex> lock(mutex);
        cout << "Exit" << endl;
        reset();
        is_ready = true;
        cv_ready.notify_all();
    });

    cv_ready.wait(lock, [this]{return is_ready;});
}
