#include <iostream>
#include <pigpiopp.h>
#include <nlohmann/json.hpp>
#include <boost/program_options.hpp>
#include <fstream>
#include "mobile-escape-room.hpp"
#include <thread>
#include <chrono>
#include <vector>
#include <string>

using namespace std;

#define HELP "help"
#define VERSION "version"
#define CFG "config"
#define INPUTS "inputs"
#define OUTPUTS "outputs"
#define RESETPIN "reset-pin"
#define EXITPIN "exit-pin"
#define SOUNDS "sounds"

int main(int argc, char *argv[])
{
    try {
        using nlohmann::json;

        namespace po = boost::program_options;
        using std::cout;
        using std::cerr;
        using std::endl;
        using std::ifstream;
        using std::vector;
        typedef std::string str;

        po::options_description generic("Generic options");
        generic.add_options()
            (HELP ",h", "produce help message")
            (VERSION ",v", "print version string")
            (CFG ",c", po::value<str>()->value_name("FILE"), "configuration file")
        ;

        /* po::options_description config("Configuration");
        config.add_options()
            (INPUTS ",i", po::value<std::vector<uns>>()->value_name("PINS"), "input pins")
            (RESETPIN ",r", po::value<uns>()->value_name("PIN"), "reset pin")
            (EXITPIN ",e", po::value<uns>()->value_name("PIN"), "exit pin")
            (OUTPUTS ",o", po::value<vector<uns>>()->value_name("PINS"), "output pins")
            (SOUNDS ",s", po::value<vector<str>>()->value_name("FILES"), "sound files")
        ; */

        po::options_description cmdline_opts;
        cmdline_opts.add(generic); // .add(config);

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, cmdline_opts), vm);

        str progname = ((argc >= 1) ? argv[0] : "NONAME");

        if (vm.count(HELP)) {
            cout << "Usage: " << progname << " [OPTION]...\n" << cmdline_opts << endl;
            return 0;
        }

        if (vm.count(VERSION)) {
            cout << progname << " version unknown"  << endl;
            return 0;
        }

        json cfg;

        if (vm.count(CFG)) {
            const str &cfg_file = vm[CFG].as<str>();
            ifstream cfg_stream(cfg_file);
            if (cfg_stream) {
                cfg = json::parse(cfg_stream);
                // po::store(boost::program_options::parse_config_file(cfg_stream, config), vm);
            }
            else {
                cerr << "Error: Cannot open config file " << cfg_file << endl;
                return 1;
            }
        }

        po::notify(vm);
        /* Labyrinth(
            std::move(vm[INPUTS].as<vector<uns>>()),
            vm[RESETPIN].as<uns>(),
            vm[EXITPIN].as<uns>(),
            std::move(vm[OUTPUTS].as<vector<uns>>()),
            std::move(vm[SOUNDS].as<vector<str>>())
        ).run(); */

        json cfg_labyrinth = cfg["labyrinth"];
        json cfg_buzzer = cfg["buzzer"];
        MobileEscapeRoom(
                cfg_labyrinth["inputs"],
                cfg_labyrinth["cheat"],
                cfg_buzzer["input"],
                cfg["reset"],
                cfg["exit"],
                cfg_labyrinth["output"],
                cfg_buzzer["output"],
                cfg_buzzer["sound"]
         ).run();
    }
    catch (const std::exception &e) {
        std::cerr << "Terminating due to exception: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}
